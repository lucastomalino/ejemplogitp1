Como usar GIT

Primero debemos clonar un repositorio
git clone https://gitlab.com/lucastomalino/ejemplogitp1.git

Consulta
git status

Agregar un archivo
git add (archivo)
git add .

Hacer commit agregando un mensaje con los cambios realizados
git commit -am "Mensaje completo y consiso"

Subir los cambios al servidor
git push origin master
git push

Bajar la ultima version
git pull origin master
git pull